/**
 * Created by kimberlykristen on 1/4/16.
 */

var express = require("express");

var app = express();

app.use(
    express.static(__dirname + "/public")
)


app.listen(3000, function() {
    console.info("Application is listening on port 3000");
});